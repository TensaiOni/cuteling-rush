#Cuteling Rush
Make cutelings rush your browser screen - plugin for firefox

Created during Ragnarson's Open Day, August 2016, as an exercise in creating web browser plugins.

Inspired by Google's Zerg rush easter egg and James Padolsey's ZergRush clone.
