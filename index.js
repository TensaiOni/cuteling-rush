var self = require("sdk/self");
var data = require("sdk/self").data;
var tabs = require("sdk/tabs");
var { attach, detach } = require('sdk/content/mod');
var { Style } = require("sdk/stylesheet/style");

var cuteling_image_url = data.url("./cuteling_icon.png");

var style = Style({
  uri: './set-game-button-style.css'
});

var button = require("sdk/ui/button/action").ActionButton({
  id: "cuteling-rush",
  label: "Cuteling Rush",
  icon: "./cuteling_icon.png",
  onClick: function() {
    require("sdk/tabs").activeTab.attach({
      contentScriptFile: ["./create-game-button.js", "./jquery-3.1.0.js"],
      contentScriptOptions: { "cuteling_image_url": cuteling_image_url }
    });
    attach(style, tabs.activeTab);
  }
});

