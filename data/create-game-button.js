var button = document.createElement("button");
var div = document.createElement("div");
div.setAttribute("class", "cuteling");
var image = document.createElement("img");
image.setAttribute("class", "cuteling");
image.setAttribute("src", self.options.cuteling_image_url);
var text = document.createTextNode("Start a wave!");
div.appendChild(image);
button.appendChild(text);
document.body.appendChild(button);
button.id = "cuteling-rush-game-start"

var global_x = 1000;
var global_y = 1000;


var doc = document,
    body = doc.body,
    atan2 = Math.atan2,
    cos = Math.cos,
    sin = Math.sin,
    sqrt = Math.sqrt,
    PI = Math.PI,
    random = Math.random,
    max = Math.max;

function CutelingWave(nCutelings) {

  var me = this,
  cutelings = this.cutelings = [],
  targets = this.targets = [],
  spawns = this.spawns = [];

  for (var i = 0; i < nCutelings; ++i) {
    cutelings.push(
        new Cuteling(
          global_x + random() * 100,
          global_y + random() * 100,
          this
          )
        );
  }

  this.intervalID = setInterval(function() {
    me.step()
  }, 30);

}

CutelingWave.prototype = {
  step: function() {

    var areFinished = true;

    for (var i = 0; i < this.cutelings.length; ++i) {
      this.cutelings[i].draw();
      areFinished = areFinished && this.cutelings[i].isFinished;
    }

    if (areFinished) {
      clearInterval(this.intervalID);
    }

  },
  destroy: function() {
    clearInterval(this.intervalID);
    for (var i = 0; i < this.cutelings.length; ++i) {
      this.cutelings[i].dom.remove();
    }
    for (var i = 0; i < this.targets.length; ++i) {
      this.targets[i].dom.css(this.targets[i].initialCSS);
      this.targets[i].dom.removeData(Cuteling.DATA_KEY);
    }
  },
  registerTarget: function(target) {
    this.targets.push(target);
  }
}

function Cuteling(x, y, wave) {
  var my_class = this;
  this.wave = wave;
  this.speed = 3;
  this.x = x;
  this.y = y;

  this.width = 10;
  this.height = 10;

  this.isAttacking = false;
  this.isFinished = false;

  var img = $('<img class="cuteling">');
  img.attr("src", self.options.cuteling_image_url);
  this.dom = $('<cuteling class="cuteling">').css({
    left: x,
    top: y,
  }).appendTo(body);
  img.appendTo(this.dom);
  this.dom.attr("hp", 10);
  var my_dom = this.dom;
  $(this.dom).click(function(){
    var me = $(this);
    var hp = me.attr("hp")
      me.attr("hp", hp - 10);
    if (hp <= 0) {
      my_class.dom.fadeOut(100, function() {
        me.remove();
      });
      my_class.dom = "";
      my_class.remove();
    }
  });
}

Cuteling.DATA_KEY = 'cutelingTargetData';
Cuteling.MAX_TARGET_AREA = 50000;
Cuteling.VISION = 10000;
Cuteling.DAMAGE = 50;

Cuteling.isSuitableTarget = function isSuitableTarget(candidate) {

  var targetData;

  if (!candidate) {
    return false;
  }

  // Make sure none of its ancestors are currently targets:
  for (var parent = candidate; parent = parent.parentNode;) {
    if ($.data(parent, Cuteling.DATA_KEY) || $(parent).hasClass("cuteling")) {
      return false;
    }
  }

  targetData = $.data(candidate, Cuteling.DATA_KEY);

  candidate = $(candidate);

  return !/cuteling/i.test(candidate[0].nodeName) &&
    (!targetData || targetData.life > 0) &&
    candidate.width() * candidate.height() < Cuteling.MAX_TARGET_AREA;

};

Cuteling.prototype = {

  calcMovement: function() {

    var target = this.target,
    xDiff = (target.position.left + random() * target.width) - this.x,
    yDiff = (target.position.top + random() * target.height) - this.y,
    angle = atan2(yDiff, xDiff);

    this.dx = this.speed * cos(angle);
    this.dy = this.speed * sin(angle);

  },

  draw: function() {
    var me = this;
    if (this.isFinished) {
      return;
    }
    if (this.dom.length < 1) {
      return;
    }

    var target = this.target;

    if (this.isAttacking) {
      if (target.life > 0) {
        target.life--;
        this.setAttacking();
        target.dom.css('opacity', target.life / Cuteling.DAMAGE);
      } else {
        target.dom.css('visibility', 'hidden');
        this.isAttacking = false;
        this.setAttacking();
        this.target = null;
      }
      return;
    }

    if (!this.target || this.target.life <= 0) {
      if (this.findTarget()) {
        target = this.target;
        this.calcMovement();
      } else {
        this.isFinished = true;
        this.dom.fadeOut(100, function() {
          $(this).remove();
        });
        return;
      }

    }

    if (this.hasReachedTarget()) {
      this.isAttacking = true;
      return;
    }

    this.x += this.dx;
    this.y += this.dy;
    this.dom.css({
      left: this.x,
      top: this.y
    });

  },

  hasReachedTarget: function() {
    var target = this.target,
    pos = target.position;

    return  this.x >= pos.left &&
      this.y >= pos.top &&
      this.x <= pos.left + target.width &&
      this.y <= pos.top + target.height
  },

  findTarget: function() {
    var targetData, radius, degree, x, y, el,
    halfWidth = this.width / 2,
    halfHeight = this.height / 2,
    scrollTop = max(body.scrollTop, doc.documentElement.scrollTop),
    scrollLeft = max(body.scrollLeft, doc.documentElement.scrollLeft);

    for (radius = 10; radius < Cuteling.VISION; radius += 50) {
      for (degree = 0; degree < 360; degree += 45) {

        x = this.x + halfWidth + radius * cos(PI/180 * degree) - scrollLeft;
        y = this.y + halfHeight + radius * sin(PI/180 * degree) - scrollTop;

        if (Cuteling.isSuitableTarget(el = doc.elementFromPoint(x, y))) {

          el = $(el);

          targetData = this.target = el.data(Cuteling.DATA_KEY);

          if (!targetData) {
            el.data(
                Cuteling.DATA_KEY,
                this.target = {
                  dom: el,
                  position: el.offset(),
                  width: el.width(),
                  height: el.height(),
                  life: 500,
                  initialCSS: {
                    visibility: '',
                    opacity: el.css('opacity') || ''
                  }
                }
                );
            this.wave.registerTarget(this.target);
          }

          return true;

        }
      }
    }

  },

  setAttacking: function() {
    if (this.isAttacking == true) {
      this.dom.css(
          {
            //background: 'yellow'
          });
    } else {
      this.dom.css(
          {
            //background: 'red'
          });
    }
  }

};

var z;
button.onclick = function() {
  z = new CutelingWave(20);
}
